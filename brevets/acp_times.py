"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def handler(control_dist_km, brevet_dist_km, num0, num1, num2):
    if control_dist_km > brevet_dist_km:
        addnum = (control_dist_km-num0)/num1
    else: addnum = (control_dist_km-num0)/num2

    return addnum

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    if 0 < control_dist_km and control_dist_km <= 200:
        num = control_dist_km/34
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if 200 < control_dist_km and control_dist_km <= 400:
        addnum = handler(control_dist_km, brevet_dist_km, 200, 34, 32)
        num = addnum + (200/34)
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if 400 < control_dist_km and control_dist_km <= 600:
        addnum = handler(control_dist_km, brevet_dist_km, 400, 32, 30)
        num = addnum + (200/34) + (200/32)
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if 600 < control_dist_km and control_dist_km <= 1000:
        addnum = handler(control_dist_km, brevet_dist_km, 600, 30, 28)
        num = addnum + (200/34) + (200/32) + (200/30)
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if 1000 < control_dist_km and control_dist_km <= 1300:
        addnum = handler(control_dist_km, brevet_dist_km, 1000, 28, 26)
        num = addnum + (200/34) + (200/32) + (200/30) + (400/28)
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if control_dist_km > 1300:
        app.logger.debug("This case shouldn't happen!")
        assert False
    

    ret = arrow.get(brevet_start_time).shift(hours=+numhour).shift(minutes=+nummin)
    return ret.isoformat()




def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if 0 < control_dist_km and control_dist_km <= 600:
        num = control_dist_km/15
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if 600 < control_dist_km and control_dist_km <= 1000:
        addnum = handler(control_dist_km, brevet_dist_km, 600, 15, 11.428)
        num = addnum + (600/15)
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if 1000 < control_dist_km and control_dist_km <= 1300:
        addnum = handler(control_dist_km, brevet_dist_km, 1000, 11.428, 13.333)
        num = addnum + (600/15) + (400/11.428)
        numhour = int(num)
        nummin = math.ceil((num%1)*60)
        
    if control_dist_km > 1300:
        app.logger.debug("This case shouldn't happen!")
        assert False
    

    ret = arrow.get(brevet_start_time).shift(hours=+numhour).shift(minutes=+nummin)
    return ret.isoformat()
